/* pause-window.c
 *
 * Copyright 2022 Shbozz
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "config.h"

#include "pause-window.h"

struct _PauseWindow
{
  AdwApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkLabel            *label;
};

G_DEFINE_FINAL_TYPE (PauseWindow, pause_window, ADW_TYPE_APPLICATION_WINDOW)

static void
pause_window_class_init (PauseWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Example/pause-window.ui");
  gtk_widget_class_bind_template_child (widget_class, PauseWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, PauseWindow, label);
}

static void
pause_window_init (PauseWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
